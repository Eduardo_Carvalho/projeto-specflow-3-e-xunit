Adaptação do projeto final do curso de Selenium da Alura, usando o framework specflow

Projeto criado usando:
-Visual Studio 2019
-.NetCore 2.2
-xUnit
-Selenium Webdriver
-Specflow 3.0

Para inicializar o projeto:
-Defina o projeto Alura.LeilaoOnline.WebApp (IIS Express) como projeto de inicialização;
-Realize o build do projeto (ctrl+shift+B);
-Abra o console de gerenciador de pacotes NuGet > projeto padrão "Alura.LeilaoOnline.WebApp" > Insira o comando  "Update-Database";
-Verificar no "SQL Server Object Explorer" se o BD foi criado;
