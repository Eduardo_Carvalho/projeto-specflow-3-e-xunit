﻿using OpenQA.Selenium;
using Xunit;
using Alura.LeilaoOnline.Selenium.Fixtures;
using Alura.LeilaoOnline.Selenium.PageObjects;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System;

namespace Alura.LeilaoOnline.Selenium.Testes
{
    [Collection("Chrome Driver")]
    public class AoOfertarLance
    {
        private IWebDriver driver;

        public AoOfertarLance(TestFixture fixture)
        {
            driver = fixture.Driver;
        }

        [Fact]
        public void DadoLoginInteressadoDeveAtualizarLanceAtual()
        {
            //arrange
            var loginPO = new LoginPO(driver);
            loginPO.Visitar();
            loginPO.PreencheFormulario("fulano@example.org", "123");
            loginPO.SubmeteFormulario();
            var DetalheLeilaoPO = new DetalheLeilaoPO(driver);
            DetalheLeilaoPO.Visitar(1); //em andamento

            //act
            DetalheLeilaoPO.OfertarLance(300);
            //assert
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(11));
            bool iguais = wait.Until(drv => DetalheLeilaoPO.LanceAtual == 300);
            
            Assert.True(iguais);
        }
    }
}
