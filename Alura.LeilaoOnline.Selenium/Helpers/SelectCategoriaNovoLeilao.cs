﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;


namespace Alura.LeilaoOnline.Selenium.Helpers
{
    public class SelectCategoriaNovoLeilao
    {
        private IWebDriver driver;
        private IWebElement selectWrapper;
        private IEnumerable<IWebElement> opcoes;
        public SelectCategoriaNovoLeilao(IWebDriver driver, By LocatorSelectWrapper)
        {
            this.driver = driver;
            selectWrapper = driver.FindElement(LocatorSelectWrapper);
            opcoes = selectWrapper.FindElements(By.CssSelector("li>span"));
        }

        public IEnumerable<IWebElement> Options => opcoes;

        private void OpenWrapper()
        {
            selectWrapper.Click();
        }

        public void SelectByText(string option)
        {
            OpenWrapper();
            opcoes
                .Where(o => o.Text.Contains(option))
                .ToList()
                .ForEach(o =>
                {
                    o.Click();
                });

        }
    }
}
