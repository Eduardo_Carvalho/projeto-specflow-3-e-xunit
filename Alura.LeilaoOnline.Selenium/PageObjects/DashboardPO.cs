﻿using OpenQA.Selenium;


namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class DashboardPO
    {
        private IWebDriver driver;
        
        
        public FiltroLeiloesPO Filtro { get;}
        public MenuLogadoPO Logout { get; }

        public DashboardPO(IWebDriver driver)
        {
            this.driver = driver;
            Filtro = new FiltroLeiloesPO(driver);
            Logout = new MenuLogadoPO(driver);

        }

        

        
    }
}
