﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class MenuLogadoPO
    {
        private IWebDriver driver;
        private By ByBotaoLogout;
        private By ByBotaoPerfil;

        public MenuLogadoPO(IWebDriver driver)
        {
            this.driver = driver;
            ByBotaoLogout = By.Id("logout");
            ByBotaoPerfil = By.Id("meu-perfil");
            
        }

        public void EfetuarLogout()
        {
            var LinkPerfil = driver.FindElement(ByBotaoPerfil);
            var LinkLogout = driver.FindElement(ByBotaoLogout);

            IAction acaoLogout = new Actions(driver)
                .MoveToElement(LinkPerfil)
                .MoveToElement(LinkLogout)
                .Click()
                .Build();

            acaoLogout.Perform();

        }
    }
}
