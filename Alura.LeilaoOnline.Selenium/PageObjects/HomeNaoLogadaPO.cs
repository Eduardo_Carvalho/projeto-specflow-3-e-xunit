﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class HomeNaoLogadaPO
    {
        private IWebDriver driver;
        public MenuNaoLogadoPO menu { get; set; }
        public HomeNaoLogadaPO(IWebDriver driver)
        {
            this.driver = driver;
            menu = new MenuNaoLogadoPO(driver);
        }
        public void Visitar()
        {
            driver.Navigate().GoToUrl("http://localhost:5000/");
        }
    }
}
