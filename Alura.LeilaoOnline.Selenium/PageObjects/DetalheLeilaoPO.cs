﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using System.Threading;
using Alura.LeilaoOnline.Selenium.Helpers;

namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class DetalheLeilaoPO
    {
        private IWebDriver driver;
        private By ByInputValor;
        private By ByBotaoOfertar;
        private By ByLanceAtual;

        public DetalheLeilaoPO(IWebDriver driver)
        {
            this.driver = driver;
            ByInputValor = By.Id("Valor");
            ByBotaoOfertar = By.Id("btnDarLance");
            ByLanceAtual = By.Id("lanceAtual");
        }

        public double LanceAtual
        {
            get
            {
                var textoAtual = driver.FindElement(ByLanceAtual).Text;
                var valor = double.Parse(textoAtual, System.Globalization.NumberStyles.Currency);
                return valor;
            }
        }

        public void Visitar(int idLeilao)
        {
            driver.Navigate().GoToUrl($"http://localhost:5000/Home/Detalhes/{idLeilao}");
        }

        public void OfertarLance(double valor)
        {
            driver.FindElement(ByInputValor).SendKeys(valor.ToString());
            driver.FindElement(ByBotaoOfertar).Click();
        }
    }
}
