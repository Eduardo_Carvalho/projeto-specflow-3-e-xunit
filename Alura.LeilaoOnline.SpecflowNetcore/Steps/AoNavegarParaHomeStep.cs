﻿using Alura.LeilaoOnline.Selenium.Fixtures;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Xunit;

namespace Alura.LeilaoOnline.Specflow.Steps
{
    [Binding]
    [Collection("Chrome Driver")]
    public sealed class AoNavegarParaHomeStep
    {
        private readonly ScenarioContext _scenarioContext;

        private IWebDriver driver;

        public AoNavegarParaHomeStep(ScenarioContext scenarioContext, TestFixture fixture)
        {
            _scenarioContext = scenarioContext;
            driver = fixture.Driver;
        }

        [Given(@"que o browser esteja aberto")]
        public void DadoQueObrowserEstejaAberto()
        {
            
        }

        [When(@"navego para o site do leilao online")]
        public void QuandoNavegoParaOSite()
        {
            driver.Navigate().GoToUrl("http://localhost:5000");
        }

        [Then(@"verei o título do site")]
        public void EntaoVereiOTituloDoSite(int result)
        {
            Assert.Contains("Leilões", driver.Title);
        }

        [Then(@"verei os campos de login")]
        public void EntaoVereiOsCamposDeLogin()
        {
            Assert.Contains("Leilões", driver.PageSource);
        }
    }
}
