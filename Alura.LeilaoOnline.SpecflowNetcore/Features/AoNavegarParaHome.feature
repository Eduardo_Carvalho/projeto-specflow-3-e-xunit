﻿Funcionalidade: AoNavegarParaHome

@smoke
Cenário: Ver o título da homepage do leilão online
	Dado que o browser esteja aberto
	Quando navego para o site do leilao online
	Então verei o título do site
	E verei os campos de login
